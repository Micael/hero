$(document).ready(function(){


    $('.content').on('click', '.arm-fight', function (e) {
        e.preventDefault();

        $.ajax({
            url:  Routing.generate('action_hero_fight_arm', {id :  $(this).attr('data-id')}),
            success: function (data) {

                if(data.pageTo)
                {
                    endFight(data.pageTo)
                }
                if(data.lost)
                {
                    endFightLost()
                }
                else
                {

                    var modal = $('.modals-buttons');


                    // 'degats' => $degats,

                    var params = " data-armor='armor' data-hits='"+data.monster.hit+"'"
                    var button = '<button '+params+' type="button" class="btn btn-default button-chance" >Utiliser la protection</button>';


                    if($('.button-chance').length)
                    {

                        $('.button-chance').replaceWith(button);
                    }else
                    {
                        modal.append(button);
                    }

                    var message = data.hero.message+'<br>'+data.monster.message

                    showNotification(true, message);
                    refreshHero();
                    refreshFight();
                }


            }
        })
    })

    ///chance
    $('.modals-buttons').on('click', '.button-chance', function (e) {

        e.preventDefault();

        var hits = $(this).attr('data-hits');
        var action  = $(this).attr('data-action')
        var armor  = $(this).attr('data-armor')
        $.ajax({
            url:  Routing.generate('action_hero_chance'),
            data : {'hits' : hits, 'action' : action, 'armor' : armor},
            method : 'POST',
            success: function (data) {

                // console.log(data)
                if(data.pageTo)
                {
                    endFight(data.pageTo)
                }
                else {

                    showNotification(data.result, data.message);
                    refreshHero();
                    refreshFight();
                }

            }
        });

    });

    $('.arm-refresh').on('click', '.blank-fight', function (e) {
        e.preventDefault();


        $.ajax({
            url:  Routing.generate('action_hero_fight_blank'),
            method : 'POST',
            success: function (data) {
                if(data.pageTo)
                {
                    endFight(data.pageTo)
                }
                else
                {

                    var modal = $('.modals-buttons');


                    // 'degats' => $degats,
                    //     'attack'
                    if(data.result.attack)
                    {
                        var params = "data-hits='"+data.result.degats+"' data-action='hits'"
                        var button = '<button '+params+' type="button" class="btn btn-default button-chance" >Utiliser la chance</button>';

                    }
                    else if (data.result.damage){

                        var params = "data-hits='"+data.result.degats+"' data-action='heal' "
                        var button = '<button '+params+' type="button" class="btn btn-default button-chance" >Utiliser la chance</button>';
                    }

                    if($('.button-chance').length)
                    {

                        $('.button-chance').replaceWith(button);
                    }else
                    {
                        modal.append(button);
                    }



                    if(data.result)
                    {

                        showNotification(data.result, data.message);
                        refreshHero();
                        refreshFight();
                    }
                    else
                    {
                        showNotification(data.result, data.message)
                    }
                }


            }
        })
    });

    // endFight('toto');
})