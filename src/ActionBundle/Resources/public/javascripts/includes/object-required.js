$(document).ready(function(){


    $('.content').on('click', '.arm-fight', function (e) {
        e.preventDefault();

        $.ajax({
            url:  Routing.generate('action_hero_fight_arm', {id :  $(this).attr('data-id')}),
            success: function (data) {

                if(data.pageTo)
                {
                    endFight(data.pageTo)
                }
                if(data.lost)
                {
                    endFightLost()
                }
                else
                {

                    var modal = $('.modals-buttons');


                    // 'degats' => $degats,

                    var params = " data-armor='armor' data-hits='"+data.monster.hit+"'"
                    var button = '<button '+params+' type="button" class="btn btn-default button-chance" >Utiliser la protection</button>';


                    if($('.button-chance').length)
                    {

                        $('.button-chance').replaceWith(button);
                    }else
                    {
                        modal.append(button);
                    }

                    var message = data.hero.message+'<br>'+data.monster.message

                    showNotification(true, message);
                    refreshHero();
                    refreshFight();
                }


            }
        })
    })


})