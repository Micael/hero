<?php

namespace ActionBundle\Controller;

use AppBundle\Entity\Hero;
use AppBundle\Entity\Monster;
use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FightController extends Controller
{

    public function chanceAction(Request $request)
    {
        $hits  = $request->get('hits');
        $actionChance  = $request->get('action');

        $action = $this->get('app.action');

        $armor = $request->get('armor');
        $heroService = $this->getHero();
        $hero = $heroService->getHero();
        $fight = $this->getFight();
        $session = new Session();

        if($armor === "armor")
        {
            $armor = $hero->getCharacteristics()->getArmor();
            if($armor->getCurrentPoint() > 0 )
            {
                $resulDice = $action->runDice(2,1,6);
                $armor->setCurrentPoint($armor->getCurrentPoint() - 1);

                if($resulDice->getTotal() < $armor->getCurrentPoint())
                {
                    $hero->getCharacteristics()
                        ->getLife()
                        ->setCurrentPoint($hero->getCharacteristics()->getLife()->getCurrentPoint() + $hits);
                    return new JsonResponse([
                        'result' => false,
                        'message' => "Les degats sont annulée vous recuperer + ".$hits
                    ]);
                }else
                {
                    return new JsonResponse([
                        'result' => false,
                        'message' => "La armure n'as pas marché"
                    ]);
                }
            }
            else
            {
                return new JsonResponse([
                    'result' => false,
                    'message' => "Vous ne pouvez pas utiliser la chance"
                ]);
            }
        }

         $chance = $hero->getCharacteristics()->getChance();
         if($chance->getCurrentPoint() > 0 )
         {
                $resulDice = $action->runDice(2,1,6);
             $chance->setCurrentPoint($chance->getCurrentPoint() - 1);

                if($resulDice->getTotal() < $chance->getCurrentPoint())
                {

                    if($actionChance == 'hits')
                    {
                        $fight->heroAttackMonster($hits);

                        if($fight->fightWin())
                        {
                            $page = $session->get('current_page');
                            $url = $this->generateUrl('page_homepage', ['slug' => $page->getPageTo()->getSlug()]);
                            $result =[
                                    'result' => true,
                                    'message' => "Vous avez gagné le combat",
                                    'pageTo' => $url
                                ];

                            $session->set('fight', []);

                        }

                        $result =[
                            'result' => true,
                            'message' => "Vous infligé ".$hits.' degats en plus'
                        ];

                    }
                    else if($actionChance == "heal")
                    {
                        $hero->getCharacteristics()
                            ->getLife()->setCurrentPoint($hero->getCharacteristics()->getLife()->getCurrentPoint() + $hits);

                        $result =[
                            'result' => true,
                            'message' => "Vous annulez l'attaque"
                        ];
                    }

                    $heroService->setHero($hero);
                    $session->set('fight', $fight);
                    return new JsonResponse($result);

                }else
                    {
                        return new JsonResponse([
                            'result' => false,
                            'message' => "La chance n'as pas marché"
                        ]);
                }
         }
         else
         {
             return new JsonResponse([
                 'result' => false,
                 'message' => "Vous ne pouvez pas utiliser la chance"
             ]);
         }
    }

    public function armAction(Arms $arms, Request $request)
    {

        $action = $this->get('app.action');
        $heroService = $this->getHero();
        $fight = $this->getFight();
        $session = new Session();

        $armsEquiped = $heroService->getHero()->getInventories()->getEquipmentInvetory()->getObjects();

        if(!isset($armsEquiped[$arms->getId()]))
        {
            return new JsonResponse([
                'result' => false,
                'message' => "Vous ne possedez pas cette arme"
            ]);
        }

        if($fight->getFightType()->getId() !== 2 )
        {
            return new JsonResponse([
                'result' => false,
                'message' => "Compbats false"
            ]);
        }

        $resultYou = $fight->heroAttackMonsterWhitArms($arms, $action, $heroService);

        //return de l'attaque

        $result['hero'] = [
            'message' => $resultYou['message'],
            'hit' => $resultYou['hits']
        ];

        $heroService->setHero($heroService->getHero());
        $session->set('fight', $fight);


        if($fight->fightWin())
        {
            $session->set('fight', []);

            $page = $session->get('current_page');
            $url = $this->generateUrl('page_homepage', ['slug' => $page->getPageTo()->getSlug()]);
            $result =[
                'result' => true,
                'message' => "Vous avez gagné le combat",
                'pageTo' => $url
            ];

        }else
        {
            $resultMonster = $fight->monsterAttackHero($action, $heroService);

            $hit  = (isset($resultMonster['attackResult']['hits'])) ? $resultMonster['attackResult']['hits'] : 0;
            $result['monster'] = [
                'message' => "Vous avez subit ".$hit." degats",
                'hit' => $hit
            ];

        }

        if(isset($resultMonster['lost']))
        {
            return new JsonResponse($resultMonster);
        }else
        {
            // vous avez subit des degats

        }

//        dump($result);exit();
        return new JsonResponse($result);


    }

    public function blankAction(Request $request)
    {
        $action = $this->get('app.action');
        $heroservice = $this->getHero();
        $hero = $heroservice->getHero();
        $fight = $this->getFight();
        $result ="" ;

        $degats = 2;

        $session = new Session();


        if($fight->getFightType()->getId() !== 1 )
        {
            return new JsonResponse([
                'result' => false,
                'message' => "Compbats false"
            ]);
        }


        $resulAttackMonster = $action->runDice(2,1,6)->getTotal() + $fight->getMonstersToFight()->getMonster()->getStrength();

        $heroResult = $action->runDice(2,1,6)->getTotal() + $hero->getCharacteristics()->getStrength()->getCurrentPoint();

        if($heroResult > $resulAttackMonster)
        {
            //you attack


            $fight->heroAttackMonster($degats);

            $result =[
                'result' => true,
                'message' => "Vous avez attaqué",
                'result' => [
                        'hero' => $heroResult,
                        'monster' => $resulAttackMonster,
                        'degats' => $degats,
                        'attack' => true,
                ]
            ];
        }
        elseif($heroResult < $resulAttackMonster)
        {
            //monster attack
            $life = $hero->getCharacteristics()->getLife();
            $life->setCurrentPoint($life->getCurrentPoint() - $degats );

//            dump($life);exit();
            if($life->getCurrentPoint() <= 0)
            {
                $result = [
                    'result' => false,
                    'message' => "Vous avez perdu le combat"

                ];

                $session->set('fight', []);
                $session->set('hero', []);

            }else
            {
                $result =[
                    'result' => true,
                    'message' => "Vous avez été attaqué",
                    'result' => [
                        'hero' => $heroResult,
                        'monster' => $resulAttackMonster,
                        'degats' => $degats,
                        'damage' => true,
                    ]
                ];
            }
        }
        else
        {

            $result =[
                'result' => true,
                'message' => "Égalité"
            ];
        }


        $heroservice->setHero($hero);
        $session->set('fight', $fight);

        if($fight->fightWin())
        {

            $page = $session->get('current_page');
            $url = $this->generateUrl('page_homepage', ['slug' => $page->getPageTo()->getSlug()]);
            $result =[
                'result' => true,
                'message' => "Vous avez gagné le combat",
                'pageTo' => $url
            ];

            $session->set('fight', []);

        }

        return new JsonResponse($result);
    }

        private function getFight()
    {
        $session =  new Session();
        return $session->get('fight');
    }

    private function getHero()
    {
        return $this->get('app.hero');
    }
}
