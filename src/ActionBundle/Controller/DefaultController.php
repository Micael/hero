<?php

namespace ActionBundle\Controller;

use AppBundle\Entity\Hero;
use AppBundle\Entity\Monster;
use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Pills;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    public  function  savedAction()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();

        $hero->setUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->clear();

        $em->merge($hero);

        $em->flush();

        return new JsonResponse(['result' => true]);
    }

    public function refreshFightAction()
    {
        $session = new Session();
        $hero = $this->getHero();

        $fight = $session->get('fight');

        $arm = $this->render('PageBundle:FightInclude:arms.html.twig',  ['hero' => $hero , 'fight' => $fight]);
        $monster = $this->render('PageBundle:FightInclude:monster.html.twig',  ['hero' => $hero , 'fight' => $fight]);
        $object = $this->render('PageBundle:FightInclude:object.html.twig',  ['hero' => $hero , 'fight' => $fight]);

        return new JsonResponse([
            'arm' => $arm->getContent(),
            'monster' => $monster->getContent(),
            'object' => $object->getContent()
        ]);
    }

    public  function pilsAction(Pills $pills)
    {
        $hero = $this->getHero();
        $heroService = $this->get('app.hero');

        $invetory =  $hero->getInventories()->getPotionInvetory();


        if(isset($invetory->getObjects()[$pills->getId()]))
            {

                $hero->getCharacteristics()->getLife()->setCurrentPoint(
                    $hero->getCharacteristics()->getLife()->getCurrentPoint() + $pills->getheals()
                );

                $invetory->getObjects()[$pills->getId()][0]->setLifePoint(
                    $invetory->getObjects()[$pills->getId()][0]->getLifePoint() - 1
                );

                $heroService->setHero($hero);

                return new JsonResponse([
                    'result' => true,
                    'message' => "Vous vous etes soignez de : ".$pills->getheals(),
                ]);

            }

         return new JsonResponse([
            'result' => true,
            'message' => "Vous n'vaez pas l'objet"
        ]);
    }
    private  function getHero()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();
        return $hero;
    }

}
