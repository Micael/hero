<?php

namespace ActionBundle\Controller;

use AppBundle\Entity\Hero;
use AppBundle\Entity\Monster;
use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use AppBundle\Entity\Objects\Pills;
use PageBundle\Entity\ChoiceType;
use PageBundle\Entity\PageChoices;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ChoiceController extends Controller
{
    public function indexAction(PageChoices $choice)
    {

        if(!$this->checkChoice($choice))
        {
            return new JsonResponse([
                'result' => false,
                'message' => "Erreur vos avec déja effectué cette action",
                'link' =>$this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])

            ]);
        }


        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();

        $result = [
            'result' => false,
            'message' => "Erreur vos avec déja effectué cette action"
        ];


        $user = $this->getUser();
        switch ($choice->getChoiceType()->getId())
        {
            case 1 :
                //inventaire required
                $result = $this->inventoryRequired($hero, $choice, $user);
            break;
            case 2 :
                //inventaire add
                $result = $this->inventoryAdd($hero, $choice, $user);
            break;
            case 3 :
                //arm add
                $result = $this->armAdd($hero, $choice, $user);
            break;
            case 4 :
                //arm ned
                $result = $this->armRequired($hero, $choice, $user);
            break;
            case 5 :
                //arm ned
                $result = $this->pillsAdd($hero, $choice, $user);
            break;
            case 7 :
                //arm ned
                $result = $this->jetDeChance($hero, $choice, $user);
                break;
            case 9 :
                $user->addChoicesDone($choice);

                $result =  [
                    'result' => true,
                    'message' => "Ils ne se passe rein ",
                    'link' => $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
                ];
            break ;

        }

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->flush();

//        dump($hero->getInventories()->getPotionInvetory()->getObjects());exit();

        $heroService->setHero($hero);

        return new JsonResponse($result);

    }

    private function inventoryRequired(Hero &$hero, PageChoices $choice, &$user)
    {
        $invetory =  $hero->getInventories()->getObjectInvetory();
        $oldObjects =  $invetory->getObjects();

        foreach ($choice->getObjectRequired()  as $objectRequired)
        {
            if(!isset($invetory->getObjects()[$objectRequired->getId()]))
            {
                $invetory->setObjects($oldObjects);
                return [
                    'result' => false,
                    'message' => "Erreur il vous manque un objet : ".$objectRequired->getName(),
                ];

            }else
            {
                $invetory->removeObject($objectRequired);
                $user->addChoicesDone($choice);
            }
        }

        return [
            'result' => true,
            'message' => "Vous pouvez Passer",
            'link' =>  $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
        ];

    }

    private function pillsAdd(Hero &$hero, PageChoices $choice, &$user)
    {
        $invetory =  $hero->getInventories()->getPotionInvetory();

        foreach($choice->getPillsAdd() as $object)
        {
            $invetory->addObject($object);

        }
        $user->addChoicesDone($choice);
        return [
            'result' => true,
            'message' => "Pilule à bien été ajouté",
            'link' => $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
        ];
    }

    private function inventoryAdd(Hero &$hero, PageChoices $choice, &$user)
    {
        $invetory =  $hero->getInventories()->getObjectInvetory();

        foreach($choice->getObjectAdd() as $object)
        {
            $invetory->addObject($object);
        }
        $user->addChoicesDone($choice);

        return [
            'result' => true,
            'message' => "Object à bien été ajouté",
            $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
        ];
    }


    private function armAdd(Hero &$hero, PageChoices $choice, &$user)
    {
        $invetory =  $hero->getInventories()->getEquipmentInvetory();
        foreach($choice->getArmAdd() as $object)
        {

            $invetory->addObject($object);
        }
        $user->addChoicesDone($choice);


        return [
            'result' => true,
            'message' => "Arme à bien été ajouté",
            'link' => $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
        ];
    }
    private function armRequired(Hero &$hero, PageChoices $choice, &$user)
    {
        $invetory =  $hero->getInventories()->getEquipmentInvetory();
        $oldObjects =  $invetory->getObjects();

        foreach ($choice->getArmRequired()  as $objectRequired)
        {
            if(!isset($invetory->getObjects()[$objectRequired->getId()]))
            {
                $invetory->setObjects($oldObjects);
                return [
                    'result' => false,
                    'message' => "Erreur il vous manque un objet : ".$objectRequired->getName(),
                ];

            }else
            {
                $invetory->removeObject($objectRequired);
                $user->addChoicesDone($choice);

            }
        }

        return [
            'result' => true,
            'message' => "Vous pouvez Passer",
            'link' => $this->generateUrl('page_homepage', ['slug' => $choice->getLink()->getSlug()])
        ];
    }

    public function jetDeChance(Hero &$hero, PageChoices $choice, &$user)
            {
                $heroService =  $this->get('app.hero');

                $hero = $heroService->getHero();
                $action = $this->get('app.action');

                $resultDice = $action->runDice(2);

                $hero->getCharacteristics()->getChance()->setCurrentPoint(
                    $hero->getCharacteristics()->getChance()->getCurrentPoint() - 1
                );
                if ($resultDice->getTotal() < $hero->getCharacteristics()->getChance()->getCurrentPoint())
                {
                    //gagne
                    $element = $choice->getChanceOk();

                    $type = explode('-', $element['object']);

                    $em = $this->getDoctrine()->getManager();
                    switch ($type[0])
                    {
                        case 'arm' :
                            $invetory =  $hero->getInventories()->getEquipmentInvetory();

                            $arm = $em->getRepository(Arms::class)->find($type[1]);
                            $invetory->addObject($arm, $element['number']);

                            return [
                                'result' => true,
                                'message' => "vous avez gagné une arme ".$arm->getName(). ' en quantité : '.$element['number'],
                                'link' => $element['page']
                            ];

                            break;
                        case 'object' :
                            $invetory =  $hero->getInventories()->getObjectInvetory();

                            $object = $em->getRepository(Object::class)->find($type[1]);
                            $invetory->addObject($object);

                            return [
                                'result' => true,
                                'message' => "vous avez gagné un nouvel objet ".$object->getName(),
                                'link' => $element['page']
                            ];

                            break;
                        case 'pil' :
                            $invetory =  $hero->getInventories()->getPotionInvetory();

                            $pil = $em->getRepository(Pills::class)->find($type[1]);
                            $invetory->addObject($pil, $element['number'],$element['number']);

                            return [
                                'result' => true,
                                'message' => "vous avez gagné un nouvelle pilule ".$pil->getName() . ' en qte : '.$element['number'],
                                'link' => $element['page']
                            ];
                        break;

                        case 'life' :

                            $hero->getCharacteristics()->getLife()->setCurrentPoint(
                                $hero->getCharacteristics()->getLife()->getCurrentPoint() + $element['number']
                            );

                            return [
                                'result' => true,
                                'message' => "vous avez gagné ".$element['number'].' de vie ',
                                'link' => $element['page']
                            ];
                        break;

                        case 'strengh' :

                            $hero->getCharacteristics()->getStrength()->setCurrentPoint(
                                $hero->getCharacteristics()->getStrength()->getCurrentPoint() + $element['number']
                            );

                            return [
                                'result' => true,
                                'message' => "vous avez gagné ".$element['number'].' de force ',
                                'link' => $element['page']
                            ];
                        break;

                        case 'armor' :

                            $hero->getCharacteristics()->getArmor()->setCurrentPoint(
                                $hero->getCharacteristics()->getArmor()->getCurrentPoint() + $element['number']
                            );

                            return [
                                'result' => true,
                                'message' => "vous avez gagné ".$element['number'].' d\'armure ',
                                'link' => $element['page']
                            ];
                        break;

                        case 'chance' :

                            $hero->getCharacteristics()->getChance()->setCurrentPoint(
                                $hero->getCharacteristics()->getChance()->getCurrentPoint() + $element['number']
                            );

                            return [
                                'result' => true,
                                'message' => "vous avez gagné ".$element['number'].' d\'armure ',
                                'link' => $element['page']
                            ];
                        break;

                        default :
                            return [
                                'message' => "Passez",
                                'link' => $element['page']
                            ];
                        break;
                    }

                }
                else {
                //gagne
                $element = $choice->getChanceNone();

                $type = explode('-', $element['object']);

                $em = $this->getDoctrine()->getManager();

                switch ($type[0]) {
                    case 'arm' :
                        $invetory = $hero->getInventories()->getEquipmentInvetory();

                        $arm = $em->getRepository(Arms::class)->find($type[1]);
                        $invetory->removeObject($arm, $element['number']);

                        return [
                            'result' => true,
                            'message' => "vous avez perdu une arme " . $arm->getName() . ' en quantité : ' . $element['number'],
                            'link' => $element['page']
                        ];

                        break;
                    case 'object' :
                        $invetory = $hero->getInventories()->getObjectInvetory();

                        $object = $em->getRepository(Object::class)->find($type[1]);
                        $invetory->removeObject($object);

                        return [
                            'result' => true,
                            'message' => "vous avez perdu un  objet " . $object->getName(),
                            'link' => $element['page']
                        ];

                        break;
                    case 'pil' :
                        $invetory = $hero->getInventories()->getPotionInvetory();

                        $pil = $em->getRepository(Pills::class)->find($type[1]);

                        $invetory->removeObject($pil, $element['number'], $element['number']);

                        return [
                            'result' => true,
                            'message' => "vous avez perdu  pilule " . $pil->getName() . ' en qte : ' . $element['number'],
                            'link' => $element['page']
                        ];
                        break;


                    case 'life' :

                        $hero->getCharacteristics()->getLife()->setCurrentPoint(
                            $hero->getCharacteristics()->getLife()->getCurrentPoint() + $element['number']
                        );

                        return [
                            'result' => true,
                            'message' => "vous avez gagné ".$element['number'].' de vie ',
                            'link' => $element['page']
                        ];
                        break;

                    case 'strengh' :

                        $hero->getCharacteristics()->getStrength()->setCurrentPoint(
                            $hero->getCharacteristics()->getStrength()->getCurrentPoint() + $element['number']
                        );

                        return [
                            'result' => true,
                            'message' => "vous avez gagné ".$element['number'].' de force ',
                            'link' => $element['page']
                        ];
                        break;

                    case 'armor' :

                        $hero->getCharacteristics()->getArmor()->setCurrentPoint(
                            $hero->getCharacteristics()->getArmor()->getCurrentPoint() + $element['number']
                        );

                        return [
                            'result' => true,
                            'message' => "vous avez gagné ".$element['number'].' d\'armure ',
                            'link' => $element['page']
                        ];
                        break;

                    case 'chance' :

                        $hero->getCharacteristics()->getChance()->setCurrentPoint(
                            $hero->getCharacteristics()->getChance()->getCurrentPoint() + $element['number']
                        );

                        return [
                            'result' => true,
                            'message' => "vous avez gagné ".$element['number'].' d\'armure ',
                            'link' => $element['page']
                        ];
                        break;


                    default :
                        return [
                            'message' => "Passez",
                            'link' => $element['page']
                        ];
                        break;
                    }
                }
            }

    private function checkChoice(PageChoices $choice)
    {
        $user = $this->getUser();

        if($user->getChoicesDone()->contains($choice))
        {
            return false;
        }

        return true;
    }

}
