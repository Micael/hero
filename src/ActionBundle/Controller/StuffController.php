<?php

namespace ActionBundle\Controller;

use AppBundle\Entity\Hero;
use AppBundle\Entity\Monster;
use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class StuffController extends Controller
{
    public function indexAction()
    {
        return $this->render('ActionBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();

        if(!$request->get('type')) {

            return new JsonResponse([ 'result' => 'false' , 'message' => 'Invalid type']);
        }

        switch ($request->get('type'))
        {
            case 'arm' :
                    if(!$request->get('id')) {

                        return new JsonResponse([ 'result' => 'false' , 'message' => 'id not found']);
                    }

                    $arm = $this->getDoctrine()->getRepository(Arms::class)->find($request->get('id'));

                    if(!$arm) {

                        return new JsonResponse([ 'result' => false , 'message' => 'arm not found']);
                    }
                    if($hero->getMoney() >=  $arm->getPrice())
                    {
                        $objects = $hero->getInventories()->getEquipmentInvetory()->getObjects();

                        if(isset($objects[$arm->getId()]) && $arm->getUnique())
                        {
                            return new JsonResponse([ 'result' => true , 'message' => 'dêjà equipé','money' => $hero->getMoney()]);

                        }
                        $hero->getInventories()->getEquipmentInvetory()->addObject($arm);
                        $hero->setMoney($hero->getMoney() - $arm->getPrice());
                        $heroService->setHero($hero);

                        return new JsonResponse([ 'result' => true , 'message' => 'arm equiped', 'money' => $hero->getMoney()]);
                    }
                    else
                    {
                        return new JsonResponse([ 'result' => true , 'message' => 'pas assez d\'argent', 'money' => $hero->getMoney()]);
                    }
                break;

            case 'object' :
                if(!$request->get('id')) {

                    return new JsonResponse([ 'result' => 'false' , 'message' => 'id not found']);
                }

                $object = $this->getDoctrine()->getRepository(Object::class)->find($request->get('id'));

                if(!$object) {

                    return new JsonResponse([ 'result' => false , 'message' => 'object not found']);
                }

                if($hero->getMoney() >=  $object->getPrice())
                {
                    $objects = $hero->getInventories()->getObjectInvetory()->getObjects();

                    if(isset($objects[$object->getId()]) && $object->getUnique())
                    {
                        return new JsonResponse([ 'result' => true , 'message' => 'object deja  equipé']);

                    }
                    $hero->getInventories()->getObjectInvetory()->addObject($object);
                    $hero->setMoney($hero->getMoney() - $object->getPrice());
                    $heroService->setHero($hero);

                    return new JsonResponse([ 'result' => true , 'message' => 'objet equiped', 'money' => $hero->getMoney()]);
                }
                else
                {
                    return new JsonResponse([ 'result' => true , 'message' => 'pas assez d\'argent', 'money' => $hero->getMoney()]);
                }
                break;

            case 'armor' :

                $value = $request->get('value');

                if($value == "delete")
                {

                    $hero->getCharacteristics()->getArmor()->setCurrentPoint(
                        $hero->getCharacteristics()->getArmor()->getCurrentPoint() - 2
                    );
                    $hero->setMoney($hero->getMoney() + 2);
                    $heroService->setHero($hero);

                    return new JsonResponse(['result' => true, 'message' => 'ok', 'money' => $hero->getMoney()]);

                }

                if($hero->getMoney() >=  2) {
                    $hero->getCharacteristics()->getArmor()->buyArmor(2);
                    $hero->setMoney($hero->getMoney() - 2);

                    $heroService->setHero($hero);
                    return new JsonResponse(['result' => true, 'message' => 'ok', 'money' => $hero->getMoney()]);
                }
                else
                {
                    return new JsonResponse([ 'result' => true , 'message' => 'pas assez d\'argent', 'money' => $hero->getMoney()]);
                }

            break;
        }

        if(!$request->get('type')) {

            return new JsonResponse([ 'result' => 'false' , 'message' => 'Unknown type']);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function removeAction(Request $request)
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();

        if(!$request->get('type')) {

            return new JsonResponse([ 'result' => 'false' , 'message' => 'Invalid type']);
        }

        switch ($request->get('type'))
        {
            case 'arm' :
                if(!$request->get('id')) {

                    return new JsonResponse([ 'result' => 'false' , 'message' => 'id not found']);
                }

                $arm = $this->getDoctrine()->getRepository(Arms::class)->find($request->get('id'));

                if(!$arm) {

                    return new JsonResponse([ 'result' => false , 'message' => 'arm not found']);
                }
                $objects = $hero->getInventories()->getEquipmentInvetory()->getObjects();


                if(isset($objects[$arm->getId()]))
                {
                    $hero->getInventories()->getEquipmentInvetory()->removeObject($arm);
                    $hero->setMoney($hero->getMoney() + $arm->getPrice());
                    $heroService->setHero($hero);
                    return new JsonResponse([ 'result' => true , 'message' => 'arme enlevée', 'money' => $hero->getMoney()]);

                }

                return new JsonResponse([ 'result' => true , 'message' => 'arme non equipé', 'money' => $hero->getMoney()]);


                break;

            case 'object' :
                if(!$request->get('id')) {

                    return new JsonResponse([ 'result' => 'false' , 'message' => 'id not found']);
                }

                $object = $this->getDoctrine()->getRepository(Object::class)->find($request->get('id'));

                if(!$object) {

                    return new JsonResponse([ 'result' => false , 'message' => 'object not found']);
                }
                    $objects = $hero->getInventories()->getObjectInvetory()->getObjects();

                    if(isset($objects[$object->getId()]))
                    {
                        $hero->getInventories()->getObjectInvetory()->removeObject($object);
                        $hero->setMoney($hero->getMoney() + $object->getPrice());
                        $heroService->setHero($hero);

                        return new JsonResponse([ 'result' => true , 'message' => 'enlever objet', 'money' => $hero->getMoney()]);

                    }


                    return new JsonResponse([ 'result' => true , 'message' => 'objet not equiped', 'money' => $hero->getMoney()]);

                break;

            case 'armor' :

                    $hero->getCharacteristics()->getArmor()->removeArmor(2);
                    $hero->setMoney($hero->getMoney() + 2);

                    $heroService->setHero($hero);
                    return new JsonResponse(['result' => true, 'message' => 'ok', 'money' => $hero->getMoney()]);


                break;
        }

        if(!$request->get('type')) {

            return new JsonResponse([ 'result' => 'false' , 'message' => 'Unknown type']);
        }
    }
}
