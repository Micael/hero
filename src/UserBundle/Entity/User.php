<?php
namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Hero",cascade={"all"}, mappedBy="user")
     */
    private $hero;

    /**
     * @ORM\JoinTable(name="choices_done")
     * @ORM\ManyToMany(targetEntity="PageBundle\Entity\PageChoices")
     */
    private $choices_done;

    /**
     * @ORM\JoinTable(name="pages_done")
     * @ORM\ManyToMany(targetEntity="PageBundle\Entity\Page")
     */
    private $pages_done;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    /**
     * Set hero
     *
     * @param \AppBundle\Entity\Hero $hero
     *
     * @return User
     */
    public function setHero(\AppBundle\Entity\Hero $hero = null)
    {
        $this->hero = $hero;

        return $this;
    }

    /**
     * Get hero
     *
     * @return \AppBundle\Entity\Hero
     */
    public function getHero()
    {
        return $this->hero;
    }


    /**
     * Add choicesDone
     *
     * @param \PageBundle\Entity\PageChoices $choicesDone
     *
     * @return User
     */
    public function addChoicesDone(\PageBundle\Entity\PageChoices $choicesDone)
    {
        $this->choices_done[] = $choicesDone;

        return $this;
    }

    /**
     * Remove choicesDone
     *
     * @param \PageBundle\Entity\PageChoices $choicesDone
     */
    public function removeChoicesDone(\PageBundle\Entity\PageChoices $choicesDone)
    {
        $this->choices_done->removeElement($choicesDone);
    }

    /**
     * Get choicesDone
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoicesDone()
    {
        return $this->choices_done;
    }

    /**
     * Add pagesDone
     *
     * @param \PageBundle\Entity\Page $pagesDone
     *
     * @return User
     */
    public function addPagesDone(\PageBundle\Entity\Page $pagesDone)
    {
        $this->pages_done[] = $pagesDone;

        return $this;
    }

    /**
     * Remove pagesDone
     *
     * @param \PageBundle\Entity\Page $pagesDone
     */
    public function removePagesDone(\PageBundle\Entity\Page $pagesDone)
    {
        $this->pages_done->removeElement($pagesDone);
    }

    /**
     * Get pagesDone
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPagesDone()
    {
        return $this->pages_done;
    }

    public function flushUser()
    {
        $this->pages_done = [];
        $this->choices_done =[];
    }
}
