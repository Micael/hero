<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use AppBundle\Entity\Objects\Pills;
use PageBundle\Entity\History;
use PageBundle\Entity\Monster;
use PageBundle\Entity\MonsterPage;
use PageBundle\Entity\Page;
use PageBundle\Entity\PageChoices;
use PageBundle\Form\PageChoicesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Page controller.
 *
 */
class PageController extends Controller
{
    /**
     * Lists all page entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pages = $em->getRepository('PageBundle:Page')->findAll();

        return $this->render('AdminBundle:Page:index.html.twig', array(
            'pages' => $pages,
        ));
    }

    /**
     * Creates a new page entity.
     *
     */
    public function newAction(Request $request, History $history)
    {
        $page = new Page();

        $templates = [
            'Theme par default' => 'PageBundle:Theme:default.html.twig',
            'Theme par default combat' => 'PageBundle:Theme:default-fight.html.twig',
        ];

        $form = $this->createForm('PageBundle\Form\PageType', $page, [
            'templates' => $templates
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $controller = ($page->getFightPage()) ? 'PageBundle:Page:fight' : 'PageBundle:Page:Object';
            $page->setController($controller);
            $page->setHistory($history);
            $em->persist($page);
            $em->flush();

            return $this->redirectToRoute('history_show', ['id' => $history->getId()]);
        }

        $monsters = $this->getDoctrine()->getRepository(Monster::class)->findAll();

        return $this->render('AdminBundle:Page:new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
            'monsters' => $monsters,
            'history' => $history,
        ));
    }

//    /**
//     * Finds and displays a page entity.
//     *
//     */
//    public function showAction(Page $page)
//    {
//        $deleteForm = $this->createDeleteForm($page);
//
//        return $this->render('page/show.html.twig', array(
//            'page' => $page,
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }

    /**
     * Displays a form to edit an existing page entity.
     * @ParamConverter("page", class="PageBundle:Page", options={"id" = "id_page"})
     */
    public function editAction(History $history, Page $page, Request $request)
    {
        $deleteForm = $this->createDeleteForm($page);

        $templates = [
            'Theme par default' => 'PageBundle:Theme:default.html.twig',
            'Theme par default combat' => 'PageBundle:Theme:default-fight.html.twig',
        ];

        $editForm = $this->createForm('PageBundle\Form\PageType', $page, [
            'templates' => $templates
        ]);
        $editForm->handleRequest($request);

        $monsters = $this->getDoctrine()->getRepository(Monster::class)->findAll();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $controller = ($page->getFightPage()) ? 'PageBundle:Page:fight' : 'PageBundle:Page:Object';

            $monsters = explode(';', $editForm->get('monsters_set')->getData());


            foreach ($monsters as $monster)
            {
                foreach($page->getMonsters() as $pMonster)
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($pMonster);
                    $em->flush();
                };

                $stats = explode(',', $monster);


                if(isset($stats[1]) && $stats[1] >= 0)
                {
                    $monster = $this->getDoctrine()->getRepository(Monster::class)->find($stats[0]);

                    $pageMonster = $this->getDoctrine()->getRepository(MonsterPage::class)
                        ->findOneBy([
                            'monster' => $monster,
                            'page' => $page,
                        ]);

                    if(!$pageMonster)
                    {

                        $pageMonster  =  new MonsterPage();

                        $pageMonster->setQte($stats[1]);

                        $pageMonster->setMonster($monster);
                        $pageMonster->setPage($page);

                        $this->getDoctrine()->getManager()->persist($pageMonster);

                    }else
                    {
                        $pageMonster->setQte($stats[1]);
                    }
                }
            }

            $page->setController($controller);
            $page->setHistory($history);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_page_edit', array('slug' => $history->getSlug(), 'id_page' => $page->getId()));
        }

        $array = [];
        if(!$page->getMonsters()->isEmpty())
        {
            foreach ($page->getMonsters() as $arrayMonster)
            {

                $array[$arrayMonster->getMonster()->getId()]= $arrayMonster->getQte();
            }
        }

        return $this->render('AdminBundle:Page:edit.html.twig', array(
            'page' => $page,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'monsters' => $monsters,
            'arrayMonster' => $array,
            'history' => $history,

        ));
    }


    /**
     * Displays a form to edit an existing page entity.
     *
     */
    public function choicesEditAction(Request $request, PageChoices $choice)
    {

        $form = $this->createForm(PageChoicesType::class,$choice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $choice->setChanceOk($request->get('to'));
            $choice->setChanceNone($request->get('no'));
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_page_choice', array('slug'=> $choice->getPage()->getHistory()->getSlug(), 'id_page' => $choice->getPage()->getId()));
        }

        $em = $this->getDoctrine()->getManager();
        return $this->render('AdminBundle:Page:choices-edit.html.twig', array(
            'form' => $form->createView(),
            'choice' => $choice,
            'pages' => $em->getRepository(Page::class)->findAll(),
            'objects' => $em->getRepository(Object::class)->findAll(),
            'pils' => $em->getRepository(Pills::class)->findAll(),
            'arms' => $em->getRepository(Arms::class)->findAll(),
        ));
    }

    /**
     * @param Request $request
     * @param History $history
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("page", class="PageBundle:Page", options={"id" = "id_page"})
     */
    public function choicesAction(Request $request, History $history,  Page $page)
    {

        $characteristic = [
            'Armor' => 1 ,
            'Chance' => 2 ,
            'Life' => 3 ,
            'Strength' => 4 ,
        ];

        $choice = new PageChoices();

        $form = $this->createForm(PageChoicesType::class, $choice);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {

            $choice->setPage($page);

            $em = $this->getDoctrine()->getManager();
            $em->persist($choice);
            $em->flush();

            return $this->redirectToRoute('admin_page_choice', array('slug' => $page->getHistory()->getSlug(), 'id_page' => $page->getId()));
        }
        $em = $this->getDoctrine()->getManager();

        return $this->render('AdminBundle:Page:choices.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
            'objects' => $em->getRepository(Object::class)->findAll(),
            'pages' => $em->getRepository(Page::class)->findAll(),
            'pils' => $em->getRepository(Pills::class)->findAll(),
            'arms' => $em->getRepository(Arms::class)->findAll(),
            'history' => $history,
        ));
    }

    /**
     * Deletes a page entity.
     *
     */
    public function deleteAction(Request $request, Page $page)
    {
        $form = $this->createDeleteForm($page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush();
        }

        return $this->redirectToRoute('admin_page_index');
    }

    /**
     * Creates a form to delete a page entity.
     *
     * @param Page $page The page entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_page_delete', array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
