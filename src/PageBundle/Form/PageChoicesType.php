<?php

namespace PageBundle\Form;

use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use AppBundle\Entity\Objects\Pills;
use PageBundle\Entity\ChoiceType;
use PageBundle\Entity\PageChoices;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageChoicesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('text')
                ->add('link')
                ->add('choiceType' ,EntityType::class,[
                        'class' => ChoiceType::class
                    ])
                ->add('objectAdd' ,EntityType::class,[
                        'class' => Object::class,
                    'multiple' => true,
                    'required' => false
                    ])
                ->add('objectRequired' ,EntityType::class,[
                        'class' => Object::class,
                        'multiple' => true,
                        'required' => false
                    ])
                ->add('armAdd' ,EntityType::class,[
                        'class' => Arms::class,
                        'multiple' => true,
                        'required' => false
                    ])
                ->add('armRequired' ,EntityType::class,[
                        'class' => Arms::class,
                        'multiple' => true,

                        'required' => false
                    ])
                ->add('pillsAdd' ,EntityType::class,[
                        'class' => Pills::class,
                        'multiple' => true,

                        'required' => false
                    ])
                ->add('quantity')
                ->remove('characteristic' ,\Symfony\Component\Form\Extension\Core\Type\ChoiceType::class,[
                    'choices' => [],
                ])
                ->remove('characteristicMax')
                ->remove('chanceMax')
        ->remove('chance_ok')
        ->remove('chance_none');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PageBundle\Entity\PageChoices'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pagebundle_pagechoices';
    }


}
