<?php

namespace PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $templates = $options['templates'];
        $builder->add('title')
        ->add('fightPage', ChoiceType::class,[
            'choices'  => array(
                'No' => false,
                'Oui' => true,
            ),
        ])
        ->add('fightType')
        ->add('monsters_set',HiddenType::class, ['mapped' => false, 'data_class' => null])
        ->add('arms_autorized')
        ->add('pageTo')
        ->add('object_autorized')
            ->add('description')
            ->add('template', ChoiceType::class,[
                'choices' => $templates,
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PageBundle\Entity\Page',
            'templates' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pagebundle_page';
    }


}
