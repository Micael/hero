<?php

namespace PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * @ORM\OneToMany(targetEntity="PageBundle\Entity\PageChoices", mappedBy="page", cascade={"remove"})
     */
    private $choices;
    /**
     * @var
     * @ORM\Column(name="fight_page", type="boolean")
     */
    private $fight_page;


    /**
     * @ORM\Column(name="template", type="string", length=255)
     */
    private $template;

    /**
     * @ORM\Column(name="controller", type="string", length=255)
     */
    private $controller;

    /**
     * @Gedmo\Slug(fields={"title", "id"})
     * @ORM\Column(length=128, unique=true)
     */

    private $slug;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="PageBundle\Entity\MonsterPage", mappedBy="page")
     */
    private $monsters;


    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Arms")
     */
    private $arms_autorized;

    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Object")
     */
    private $object_autorized;



    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\FightType")
     */
    private $fightType;

    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\Page", fetch="EAGER")
     */
    private $pageTo;


    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\History", inversedBy="pages")
     */
    private $history;


    /**
     * @ORM\Column(name="background", type="string", length=255, nullable=true)
     */
    private $background;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->arms_autorized = new \Doctrine\Common\Collections\ArrayCollection();
        $this->monsters = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add choice
     *
     * @param \PageBundle\Entity\PageChoices $choice
     *
     * @return Page
     */
    public function addChoice(\PageBundle\Entity\PageChoices $choice)
    {
        $this->choices[] = $choice;

        return $this;
    }

    /**
     * Remove choice
     *
     * @param \PageBundle\Entity\PageChoices $choice
     */
    public function removeChoice(\PageBundle\Entity\PageChoices $choice)
    {
        $this->choices->removeElement($choice);
    }

    /**
     * Get choices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Page
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->title;
    }

    /**
     * Add monster
     *
     * @param \PageBundle\Entity\Monster $monster
     *
     * @return Page
     */
    public function addMonster(\PageBundle\Entity\MonsterPage $monster)
    {
        $this->monsters[] = $monster;

        return $this;
    }

    /**
     * Remove monster
     *
     * @param \PageBundle\Entity\Monster $monster
     */
    public function removeMonster(\PageBundle\Entity\MonsterPage $monster)
    {
        $this->monsters->removeElement($monster);
    }


    /**
     * Get monsters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonsters()
    {
        return $this->monsters;
    }

    /**
     * Add armsAutorized
     *
     * @param \AppBundle\Entity\Objects\Arms $armsAutorized
     *
     * @return Page
     */
    public function addArmsAutorized(\AppBundle\Entity\Objects\Arms $armsAutorized)
    {
        $this->arms_autorized[] = $armsAutorized;

        return $this;
    }

    /**
     * Remove armsAutorized
     *
     * @param \AppBundle\Entity\Objects\Arms $armsAutorized
     */
    public function removeArmsAutorized(\AppBundle\Entity\Objects\Arms $armsAutorized)
    {
        $this->arms_autorized->removeElement($armsAutorized);
    }

    /**
     * Get armsAutorized
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArmsAutorized()
    {
        return $this->arms_autorized;
    }

    /**
     * Set fightType
     *
     * @param \PageBundle\Entity\FightType $fightType
     *
     * @return Page
     */
    public function setFightType(\PageBundle\Entity\FightType $fightType = null)
    {
        $this->fightType = $fightType;

        return $this;
    }

    /**
     * Get fightType
     *
     * @return \PageBundle\Entity\FightType
     */
    public function getFightType()
    {
        return $this->fightType;
    }

    /**
     * Set controller
     *
     * @param string $controller
     *
     * @return Page
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fightPage
     *
     * @param boolean $fightPage
     *
     * @return Page
     */
    public function setFightPage($fightPage)
    {
        $this->fight_page = $fightPage;

        return $this;
    }

    /**
     * Get fightPage
     *
     * @return boolean
     */
    public function getFightPage()
    {
        return $this->fight_page;
    }

    /**
     * Add objectAutorized
     *
     * @param \AppBundle\Entity\Objects\Object $objectAutorized
     *
     * @return Page
     */
    public function addObjectAutorized(\AppBundle\Entity\Objects\Object $objectAutorized)
    {
        $this->object_autorized[] = $objectAutorized;

        return $this;
    }

    /**
     * Remove objectAutorized
     *
     * @param \AppBundle\Entity\Objects\Object $objectAutorized
     */
    public function removeObjectAutorized(\AppBundle\Entity\Objects\Object $objectAutorized)
    {
        $this->object_autorized->removeElement($objectAutorized);
    }

    /**
     * Get objectAutorized
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectAutorized()
    {
        return $this->object_autorized;
    }

    /**
     * Set pageTo
     *
     * @param \PageBundle\Entity\Page $pageTo
     *
     * @return Page
     */
    public function setPageTo(\PageBundle\Entity\Page $pageTo = null)
    {
        $this->pageTo = $pageTo;

        return $this;
    }

    /**
     * Get pageTo
     *
     * @return \PageBundle\Entity\Page
     */
    public function getPageTo()
    {
        return $this->pageTo;
    }

    /**
     * Set background
     *
     * @param string $background
     *
     * @return Page
     */
    public function setBackground($background)
    {
        $this->background = $background;

        return $this;
    }

    /**
     * Get background
     *
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * Set history
     *
     * @param \PageBundle\Entity\History $history
     *
     * @return Page
     */
    public function setHistory(\PageBundle\Entity\History $history = null)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get history
     *
     * @return \PageBundle\Entity\History
     */
    public function getHistory()
    {
        return $this->history;
    }
}
