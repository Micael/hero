<?php

namespace PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MonsterPage
 *
 * @ORM\Table(name="monster_page")
 * @ORM\Entity(repositoryClass="PageBundle\Repository\MonsterPageRepository")
 */
class MonsterPage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\Monster")
     */
    private $monster;

    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\Page", inversedBy="monsters")
     */

    private $page;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return MonsterPage
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set monster
     *
     * @param \PageBundle\Entity\Monster $monster
     *
     * @return MonsterPage
     */
    public function setMonster(\PageBundle\Entity\Monster $monster = null)
    {
        $this->monster = $monster;

        return $this;
    }

    /**
     * Get monster
     *
     * @return \PageBundle\Entity\Monster
     */
    public function getMonster()
    {
        return $this->monster;
    }

    /**
     * Set page
     *
     * @param \PageBundle\Entity\Page $page
     *
     * @return MonsterPage
     */
    public function setPage(\PageBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \PageBundle\Entity\Page
     */
    public function getPage()
    {
        return $this->page;
    }

}
