<?php

namespace PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Choices
 *
 * @ORM\Table(name="page_choices")
 * @ORM\Entity(repositoryClass="PageBundle\Repository\ChoicesRepository")
 */
class PageChoices
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\Page")
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\Page", inversedBy="choices")
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="PageBundle\Entity\ChoiceType")
     */
    private $choice_type;



    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * object to hill
     * @ORM\JoinTable(name="choices_object_add")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Object")
     *
     */
    private $object_add;

    /**
     * @var object to hill
     * @ORM\JoinTable(name="choices_object_required")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Object")
     */
    private $object_required;

    /**
     * @var object to hill
     * @ORM\JoinTable(name="choices_arms_required")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Arms")
     */
    private $arm_required;
    /**
     * @var object to hill
     * @ORM\JoinTable(name="choices_arms_add")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Arms")
     */
    private $arm_add;

    /**
     * @var object to hill
     * @ORM\JoinTable(name="choices_pills_add")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Objects\Pills")
     */
    private $pills_add;

    /**
     * @ORM\Column(name="characteristic", type="array", nullable=true)
     */
    private $characteristic;

    /**
     * @ORM\Column(name="characteristic_max", type="integer", nullable=true)
     */
    private $characteristic_max;

    /**
     * @ORM\Column(name="chance_max", type="integer", nullable=true)
     */
    private $chance_max;

    /**
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;


    /**
     * @ORM\Column(name="hash", length=128, unique=true)
     */

    private $hash;


    /**
     * @ORM\Column(name="chance_ok", type="array")
     */
    private $chance_ok;

    /**
     * @ORM\Column(name="chance_none", type="array")
     */
    private $chance_none;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Choices
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Choices
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set page
     *
     * @param \AppBundle\Entity\Page $page
     *
     * @return Choices
     */
    public function setPage(\PageBundle\Entity\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \AppBundle\Entity\Pagee
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set action
     *
     * @param array $action
     *
     * @return PageChoices
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return array
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set choiceType
     *
     * @param \PageBundle\Entity\ChoiceType $choiceType
     *
     * @return PageChoices
     */
    public function setChoiceType(\PageBundle\Entity\ChoiceType $choiceType = null)
    {
        $this->choice_type = $choiceType;

        return $this;
    }

    /**
     * Get choiceType
     *
     * @return \PageBundle\Entity\ChoiceType
     */
    public function getChoiceType()
    {
        return $this->choice_type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->object_add = new \Doctrine\Common\Collections\ArrayCollection();
        $this->object_required = new \Doctrine\Common\Collections\ArrayCollection();
        $this->arm_required = new \Doctrine\Common\Collections\ArrayCollection();
        $this->arm_add = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pills_add = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hash = $this->genHash(20);
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return PageChoices
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add objectAdd
     *
     * @param \AppBundle\Entity\Objects\Object $objectAdd
     *
     * @return PageChoices
     */
    public function addObjectAdd(\AppBundle\Entity\Objects\Object $objectAdd)
    {
        $this->object_add[] = $objectAdd;

        return $this;
    }

    /**
     * Remove objectAdd
     *
     * @param \AppBundle\Entity\Objects\Object $objectAdd
     */
    public function removeObjectAdd(\AppBundle\Entity\Objects\Object $objectAdd)
    {
        $this->object_add->removeElement($objectAdd);
    }

    /**
     * Get objectAdd
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectAdd()
    {
        return $this->object_add;
    }

    /**
     * Add objectRequired
     *
     * @param \AppBundle\Entity\Objects\Object $objectRequired
     *
     * @return PageChoices
     */
    public function addObjectRequired(\AppBundle\Entity\Objects\Object $objectRequired)
    {
        $this->object_required[] = $objectRequired;

        return $this;
    }

    /**
     * Remove objectRequired
     *
     * @param \AppBundle\Entity\Objects\Object $objectRequired
     */
    public function removeObjectRequired(\AppBundle\Entity\Objects\Object $objectRequired)
    {
        $this->object_required->removeElement($objectRequired);
    }

    /**
     * Get objectRequired
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObjectRequired()
    {
        return $this->object_required;
    }

    /**
     * Add armRequired
     *
     * @param \AppBundle\Entity\Objects\Arms $armRequired
     *
     * @return PageChoices
     */
    public function addArmRequired(\AppBundle\Entity\Objects\Arms $armRequired)
    {
        $this->arm_required[] = $armRequired;

        return $this;
    }

    /**
     * Remove armRequired
     *
     * @param \AppBundle\Entity\Objects\Arms $armRequired
     */
    public function removeArmRequired(\AppBundle\Entity\Objects\Arms $armRequired)
    {
        $this->arm_required->removeElement($armRequired);
    }

    /**
     * Get armRequired
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArmRequired()
    {
        return $this->arm_required;
    }

    /**
     * Add armAdd
     *
     * @param \AppBundle\Entity\Objects\Arms $armAdd
     *
     * @return PageChoices
     */
    public function addArmAdd(\AppBundle\Entity\Objects\Arms $armAdd)
    {
        $this->arm_add[] = $armAdd;

        return $this;
    }

    /**
     * Remove armAdd
     *
     * @param \AppBundle\Entity\Objects\Arms $armAdd
     */
    public function removeArmAdd(\AppBundle\Entity\Objects\Arms $armAdd)
    {
        $this->arm_add->removeElement($armAdd);
    }

    /**
     * Get armAdd
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArmAdd()
    {
        return $this->arm_add;
    }

    /**
     * Add pillsAdd
     *
     * @param \AppBundle\Entity\Objects\Pills $pillsAdd
     *
     * @return PageChoices
     */
    public function addPillsAdd(\AppBundle\Entity\Objects\Pills $pillsAdd)
    {
        $this->pills_add[] = $pillsAdd;

        return $this;
    }

    /**
     * Remove pillsAdd
     *
     * @param \AppBundle\Entity\Objects\Pills $pillsAdd
     */
    public function removePillsAdd(\AppBundle\Entity\Objects\Pills $pillsAdd)
    {
        $this->pills_add->removeElement($pillsAdd);
    }

    /**
     * Get pillsAdd
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPillsAdd()
    {
        return $this->pills_add;
    }

    public function action()
    {
        //return  action of choices
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PageChoices
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set characteristic
     *
     * @param array $characteristic
     *
     * @return PageChoices
     */
    public function setCharacteristic($characteristic)
    {
        $this->characteristic = $characteristic;

        return $this;
    }

    /**
     * Get characteristic
     *
     * @return array
     */
    public function getCharacteristic()
    {
        return $this->characteristic;
    }

    /**
     * Set characteristicMax
     *
     * @param integer $characteristicMax
     *
     * @return PageChoices
     */
    public function setCharacteristicMax($characteristicMax)
    {
        $this->characteristic_max = $characteristicMax;

        return $this;
    }

    /**
     * Get characteristicMax
     *
     * @return integer
     */
    public function getCharacteristicMax()
    {
        return $this->characteristic_max;
    }

    /**
     * Set chanceMax
     *
     * @param integer $chanceMax
     *
     * @return PageChoices
     */
    public function setChanceMax($chanceMax)
    {
        $this->chance_max = $chanceMax;

        return $this;
    }

    /**
     * Get chanceMax
     *
     * @return integer
     */
    public function getChanceMax()
    {
        return $this->chance_max;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return PageChoices
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    private function genHash($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Set chanceOk
     *
     * @param array $chanceOk
     *
     * @return PageChoices
     */
    public function setChanceOk($chanceOk)
    {
        $this->chance_ok = $chanceOk;

        return $this;
    }

    /**
     * Get chanceOk
     *
     * @return array
     */
    public function getChanceOk()
    {
        return $this->chance_ok;
    }

    /**
     * Set chanceNone
     *
     * @param array $chanceNone
     *
     * @return PageChoices
     */
    public function setChanceNone($chanceNone)
    {
        $this->chance_none = $chanceNone;

        return $this;
    }

    /**
     * Get chanceNone
     *
     * @return array
     */
    public function getChanceNone()
    {
        return $this->chance_none;
    }
}
