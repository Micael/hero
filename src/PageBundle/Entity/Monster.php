<?php

namespace PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Monster
 *
 * @ORM\Table(name="monster")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MonsterRepository")
 */
class Monster
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="life", type="integer")
     */
    private $life;


    /**
     * @var int
     *
     * @ORM\Column(name="based_life", type="integer")
     */
    private $based_life;

    /**
     * @var int
     *
     * @ORM\Column(name="strength", type="integer")
     */
    private $strength;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Objects\Arms")
     */
    private $arm;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Monster
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set life
     *
     * @param integer $life
     *
     * @return Monster
     */
    public function setLife($life)
    {
        $this->life = $life;

        return $this;
    }

    /**
     * Get life
     *
     * @return int
     */
    public function getLife()
    {
        return $this->life;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return mixed
     */
    public function getArm()
    {
        return $this->arm;
    }

    /**
     * @param mixed $arm
     */
    public function setArm($arm)
    {
        $this->arm = $arm;
    }

    /**
     * @return int
     */
    public function getBasedLife()
    {
        return $this->based_life;
    }

    /**
     * @param int $based_life
     */
    public function setBasedLife($based_life)
    {
        $this->based_life = $based_life;
    }


    public function  __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

}
