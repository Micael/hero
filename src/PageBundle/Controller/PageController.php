<?php

namespace PageBundle\Controller;


use AppBundle\Modal\Fight;
use PageBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class PageController extends Controller
{

    public function indexAction(Page $page)
    {
        $hero = $this->getHero();

//        dump($hero);exit();

        if(!$hero || $hero->getCharacteristics()->getLife()->getCurrentPoint() <= 0 )
        {
            return $this->redirectToRoute('app_card');
        }
        return $this->render('PageBundle:Page:index.html.twig', [
            'page' => $page,
            'hero' => $this->getHero()
        ]);
    }

    public function ObjectAction(Page $page)
    {
        $hero = $this->getHero();
        if(!$hero || $hero->getCharacteristics()->getLife()->getCurrentPoint() <= 0 )
        {
            return $this->redirectToRoute('app_card');
        }

        return $this->render($page->getTemplate(), [
            'page' => $page
        ]);
    }

    public  function fightAction(Page $page)
    {

        $session = new Session();
        $hero = $this->getHero();

        if(!$hero || $hero->getCharacteristics()->getLife()->getCurrentPoint() <= 0 )
        {
            return $this->redirectToRoute('app_card');
        }
//        dump($hero);exit();
        $session->set('current_page',$page);
//        $session->set('fight',[]);
        if(!empty($session->get('fight')))
        {
            $fight = $session->get('fight');
        }
        else{

            $fight = new Fight();

            $fight->setArmsAutorized($page->getArmsAutorized()->toArray());
            $fight->setObjectAutorized($page->getObjectAutorized()->toArray());
            $fight->setMonsters($page->getMonsters()->toArray());
            $fight->setFightType($page->getFightType());

            $session->set('fight', $fight);

        }

        return $this->render($page->getTemplate(), ['hero' => $hero , 'fight' => $fight]);

    }

    private  function getHero()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();
        return $hero;
    }

}
