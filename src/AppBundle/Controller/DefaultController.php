<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Characteristic;
use AppBundle\Entity\Characteristics\Armor;
use AppBundle\Entity\Characteristics\Chance;
use AppBundle\Entity\Characteristics\Life;
use AppBundle\Entity\Characteristics\Strength;
use AppBundle\Entity\Hero;
use AppBundle\Entity\Inventories\EquipmentInventory;
use AppBundle\Entity\Inventories\ObjectInventory;
use AppBundle\Entity\Inventories\PotionInventory;
use AppBundle\Entity\Inventory;
use AppBundle\Entity\Objects\Arms;
use AppBundle\Entity\Objects\Object;
use AppBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle:Default:index.html.twig');
    }


    public function cardAction()
    {
        /// somme d'argent

        $hero = new  Hero();
        $session = new Session();


        $session->set('fight', []);
        $session->set('hero', []);

        $actionService  =  $this->get('app.action');

        $hero->setName('toto');

        $charateristics = new Characteristic();


        $armor = new Armor();
        $armor->setPriceImprovement(0.5)
            ->setStartPoint($actionService->runDice()->firstRound() + 6);


        $chance = new Chance();
        $chance->setStartPoint($actionService->runDice()->firstRound() + 6);

        $life = new Life();
        $life->setStartPoint($actionService->runDice(2)->getTotal() + 12);


        $strength = new Strength();
        $strength->setStartPoint($actionService->runDice()->firstRound() + 6);

        $charateristics->setArmor($armor);
        $charateristics->setLife($life);
        $charateristics->setChance($chance);
        $charateristics->setStrength($strength);

        $hero->setCharacteristics($charateristics);
        $hero->setMoney(6);

        $inventory = new Inventory();



        //set invetories

        $equipementInvetory = new EquipmentInventory();
        $objectInventory  = new ObjectInventory();
        $potionInventory  = new PotionInventory();

        $equipementInvetory->setPlace(null);
        $objectInventory->setPlace(5);
        $potionInventory->setPlace(null);


        $inventory->setEquipmentInvetory($equipementInvetory);
        $inventory->setObjectInvetory($objectInventory);
        $inventory->setPotionInvetory($potionInventory);

        $session->set('hero', $hero);

        $hero->setInventories($inventory);


        return $this->render('AppBundle:Default:card.html.twig',
            [
                'hero' => $hero
            ]
        );

    }

    public function inventoryAction()
    {

        #remove choice
        $user = $this->getUser();
        $user->flushUser();
        $this->getDoctrine()->getManager()->flush();
        $session = new Session();

        $hero = $this->getHero();

        $arms = $this->getDoctrine()->getRepository(Arms::class)->findAll();
        $antimatiere = $this->getDoctrine()->getRepository(Object::class)->find(1);

        return $this->render('AppBundle:Default:inventory.html.twig',
            [
                'hero' => $hero,
                'antimatiere' => $antimatiere,
                'arms' => $arms,
                'inventories' => $hero->getInventories()
            ]);
    }

    public function showCardHero()
    {
        $hero = $this->getHero();


        return $this->render('AppBundle:Include:card.html.twig',
            [
                'hero' => $hero
            ]
        );
    }

    /**
     * @param Inventory $inventory
     * @return \Symfony\Component\HttpFoundation\Response
     * render controller method
     */
    public function showInventoryAction(Inventory $inventory)
    {

        return $this->render('AppBundle:Include:inventory.html.twig',
            [
                'inventories' => $inventory
            ]
        );
    }


    /**
     * Ajax method refresh all menus inventory and hero card
     */
    public function refreshHeroAction()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();

        $inventories = $this->render('AppBundle:Include:inventory.html.twig', ['inventories' => $hero->getInventories()]);
        $hero = $this->render('AppBundle:Include:card.html.twig', ['hero' => $hero]) ;



        return new JsonResponse([
            'result' => true,
            'data' => [
                'inventories' => $inventories->getContent(),
                'hero' => $hero->getContent(),
            ]
        ]);
    }


    /**
     * Ajax method refresh all menus inventory and hero card
     */
    public function refreshInvetoryAction()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();
        $antimatiere = $this->getDoctrine()->getRepository(Object::class)->find(1);


        $inventories = $this->render('AppBundle:Include:inventory-card.html.twig', [
            'inventories' => $hero->getInventories(),
            'antimatiere' => $antimatiere
        ]);



        return new JsonResponse([
            'result' => true,
            'data' => [
                'inventories' => $inventories->getContent(),
            ]
        ]);
    }



    private  function getHero()
    {
        $heroService =  $this->get('app.hero');

        $hero = $heroService->getHero();
        return $hero;
    }

    public function gameAction()
    {
        $hero = $this->getHero();
        return $this->render('AppBundle:Default:index.html.twig', ['hero' => $hero]);
    }

}
