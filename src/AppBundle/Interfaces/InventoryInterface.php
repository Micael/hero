<?php

namespace AppBundle\Interfaces;


interface InventoryInterface
{
    public function getObjects();

    public function getPlace();
}

