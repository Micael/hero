<?php

namespace AppBundle\Interfaces;


interface ObjectsInterface

{
    public function getName();

    public function getDescribe();

    public function getPrice();

    public function getHits();

    public function getLifePoint();

    public function DefinitionsHits();
}