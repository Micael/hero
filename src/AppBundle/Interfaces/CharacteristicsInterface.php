<?php

namespace AppBundle\Interfaces;


interface CharacteristicsInterface
{


    /**
     * Set startPoint
     *
     * @param integer $startPoint
     *
     * @return Characteristic
     */
    public function setStartPoint($startPoint);

    /**
     * Get startPoint
     *
     * @return int
     */
    public function getStartPoint();
    /**
     * Set currentPoint
     *
     * @param integer $currentPoint
     *
     * @return Characteristic
     */
    public function setCurrentPoint($currentPoint);
    /**
     * Get currentPoint
     *
     * @return int
     */
    public function getCurrentPoint();

    /**
     * Set priceImprovement
     *
     * @param integer $priceImprovement
     *
     * @return Characteristic
     */
    public function setPriceImprovement($priceImprovement);

    /**
     * Get priceImprovement
     *
     * @return int
     */
    public function getPriceImprovement();
}

