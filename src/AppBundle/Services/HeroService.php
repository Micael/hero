<?php

namespace AppBundle\Services;



use AppBundle\AppBundle;
use AppBundle\Entity\Hero;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class HeroService


{
    private $hero;
    private $session;

    public  function __construct(Session $session, TokenStorage $tokenStorage)
    {
        $this->hero = $session->get('hero');
        $this->session = $session;
        $user = $tokenStorage->getToken()->getUser();


//            if($user)
//            {
//
//                if($user->getHero())
//                {
//
//                    $this->hero = $user->getHero();
//                    $session->set('hero', $this->hero);
//                }
//            }


    }

    /**
     * @return Hero
     */
    public function getHero()
    {
        return $this->hero;
    }

    /**
     * @param mixed $hero
     */
    public function setHero($hero)
    {
        $this->session->set('hero', $hero);
        $this->hero = $hero;
    }

}
