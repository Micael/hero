<?php

namespace AppBundle\Services;


use AppBundle\Entity\Dice;

class ActionService


{

    public function ArmsFight()
    {

    }

    public function HandFight()
    {

    }

    public function runDice($number = 1,$min = 1, $max = 6)
    {
        $dice  = new Dice($number);

        for($i = 0; $i < $number; $i++)
        {
            $dice->addResult(rand($min, $max));
        }

        return $dice ;
    }
}