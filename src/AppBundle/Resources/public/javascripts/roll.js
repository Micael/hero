var app;

(function() {

  app = {
    settings: {

      elements: {},
      
      rand: '',

      configs: {
        string: "Appuyer sur le bouton pour lancer le dés",
        choiceNumber: 6,
        loop: 0,
        howManyTimes: 67,
        array_hexagon_suite: [1, 3, 5, 4, 2, 0],
        index_hexagon_suite: 0,
        delay_hexagon_suite: 50,
        array_Color_hexagon_suite: ['#2d383c', '#5b666a', '#9aa2a5', '#b0b5b9', '#c7cbce', '#dfe3e6'],
        color_hexagon_suite: '#2d383c',
      },

    },

    init: function() {
      app.parseStringContentText();
      app.displayChoiceBlock(confs.choiceNumber);
    },

    parseStringContentText: function() {

      if (app.settings.configs.string.length > 900) {
        document.getElementById('text_page').innerHTML = info;
      }
      var segments = app.settings.configs.string.match(/.{1,180}/g);

      for (var i = 0; i < segments.length; i++) {
        var elDiv = document.createElement('div');
        elDiv.className = 'carousel-item text-center p-4';

        if (i == 0) {
          elDiv.className += ' active';
        }
        var elP = document.createElement('p');
        elP.textContent = segments[i];
        elDiv.appendChild(elP);
        document.getElementsByClassName('carousel-inner')[0].appendChild(elDiv);
      }
    },

    displayChoiceBlock: function(number) {
      var disabled = 6 - number;

      for (var i = 0; i < disabled; i++) {
        document.getElementsByClassName('choice_block')[i].className += ' disabled';
      }
    },

    launchRoll: function(number) {
      document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Les dés sont jetés';
      app.newRoll();
    },

    newRoll: function() {
      confs.loop++;

      if (confs.loop < confs.howManyTimes) {
        document.getElementsByTagName('path')[confs.array_hexagon_suite[confs.index_hexagon_suite]].style.fill = confs.color_hexagon_suite;
        confs.index_hexagon_suite++;

        if (confs.loop > 55) {
          confs.delay_hexagon_suite += 50;
        }

        if (confs.index_hexagon_suite == 6) {
          confs.index_hexagon_suite = 0;
          var before_color_hexagon_suite = confs.color_hexagon_suite;
          var after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];

          while (before_color_hexagon_suite == after_color_hexagon_suite) {
            after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];
          }
          confs.color_hexagon_suite = after_color_hexagon_suite;
        }

        setTimeout(function() {
          app.newRoll();
        }, confs.delay_hexagon_suite);

      } else {
        var rand = app.randomIntFromInterval(1, 6);

        if (app.settings.rand == rand) {
          app.settings.rand = rand;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez encore obtenu un ' + app.settings.rand;
        } else {
          app.settings.rand = rand;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez obtenu un ' + app.settings.rand;
        }
        document.getElementsByClassName('contentRandNumber')[0].innerHTML = app.settings.rand;
      }

    },
    randomIntFromInterval: function(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    },
  };

  var confs = app.settings.configs;
  app.init();

})();
