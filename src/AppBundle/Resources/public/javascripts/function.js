function refreshHero()
{
    $.ajax({
        url:  Routing.generate('action_hero_refresh'),
        success: function (data) {

            if(data.result)
            {
                $('#hero-card').html(data.data.hero);
                $('#hero-inventories').html(data.data.inventories)


            }

        }
    })
}
function refreshInvetory()
{
    $.ajax({
        url:  Routing.generate('action_inventory_refresh'),
        success: function (data) {

            if(data.result)
            {
                $('.inventory-equiped').html(data.data.inventories);
                console.log(data.money)

            }

        }
    })
}

function refreshFight()
{
    $.ajax({
        url:  Routing.generate('action_hero_refresh_fight'),
        success: function (data) {

                $('.monster-refresh').html(data.monster);
                $('.arm-refresh').html(data.arm);
                $('.object-refresh').html(data.object);
                console.log('fight')

        }
    })
}


function showNotification(result , message, link_target)
{
    var content  = $('.notification');

    content.html();

    if(result)
    {
        var html =  '<p class="alert alert-success">'+message+'</p>'
    }
    else
    {
        var html =  '<p class="alert alert-danger">'+message+'</p>'
    }


    if(link_target)
    {
         html += ' <a class="btn btn-warning float-right" href="'+link_target+'">Suite</a>'
    }
    content.html(html);

    $('#notification-modal').modal('show')

}

function endFight(url)
{
    $(".content-fight").html("");
    $(".content-fight").html("<a class='btn btn-warning' href='"+url+"'>Combats gagné page suivante</a>");

}
function endFightLost()
{
    $(".content-fight").html("T'as perdu noob");

}