var app;

(function () {

    app = {
        settings: {

          elements: {
            // elButtonCircleRight: document.getElementById('circle_right'),
            // elButtonCircleLeft: document.getElementById('circle_left'),
            // elButtonInventoryWeapon: document.getElementById('inventory_weapon'),
            // elButtonInventoryObject: document.getElementById('inventory_object'),
          },

          configs: {
            string : "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            choiceNumber : 4,
          },

      },

      init: function() {
        app.parseStringContentText();
        app.displayChoiceBlock(app.settings.configs.choiceNumber);
      },

      parseStringContentText: function() {
        if (app.settings.configs.string.length > 900) {
          document.getElementById('text_page').innerHTML = info;
        }
        var segments = app.settings.configs.string.match(/.{1,180}/g);
        for (var i = 0; i < segments.length; i++)
        {
          var elDiv = document.createElement('div');
          elDiv.className = 'carousel-item text-center p-4';
          if (i == 0) {
            elDiv.className += ' active';
          }
          var elP = document.createElement('p');
          elP.textContent = segments[i];
          elDiv.appendChild(elP);
          document.getElementsByClassName('carousel-inner')[0].appendChild(elDiv);
        }
      },

      displayChoiceBlock: function(number) {
        console.log( document.getElementsByClassName('choice_block'));
        var disabled = 6 - number;
        console.log( disabled);
        for (var i = 0; i < disabled; i++) {
          document.getElementsByClassName('choice_block')[i].className += ' disabled';
        }
      },

  };

  app.init();

  // console.log('here', html);
  // document.getElementById('text_page').innerHTML = html.textContent;

    // function displayInventory(open, closed){
  //   if (document.getElementById(open).style.display == 'block') {
  //     document.getElementById(open).style.display = 'none';
  //     return;
  //   }
  //   document.getElementById(closed).style.display = 'none';
  //   document.getElementById(open).style.display = 'block';
  // }
}) ();
