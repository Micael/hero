// Routing.generate('my_route_to_expose_with_defaults', { id: 2, foo: "bar" });


$(document).ready(function () {
    $('.choice').on('click', function(e){
        e.preventDefault();

        var id = $(this).data('id')
        var type = $(this).data('type')
        var value = $(this).data('value')

        console.log(type);

        $.ajax({
            url:  Routing.generate('action_stuff_add'),
            method : 'POST',
            data : {id: id , type : type, value: value} ,
            success: function (data) {

                    if(data.result)
                    {
                        refreshHero();
                        refreshInvetory();
                        if(data)
                        {
                            $('.show-money').html('Points : '+data.money)
                        }
                    }
            }
        })
    })

    $('.inventory-equiped').on('click', '.delete-choice', function(e){
        e.preventDefault();

        var id = $(this).data('id')
        var type = $(this).data('type')

        console.log(type);

        $.ajax({
            url:  Routing.generate('action_stuff_remove'),
            method : 'POST',
            data : {id: id , type : type} ,
            success: function (data) {

                if(data.result)
                {
                    refreshHero();
                    refreshInvetory();
                    if(data)
                    {
                        $('.show-money').html('Points : '+data.money)
                    }
                }
            }
        })
    })
});