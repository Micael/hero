<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Characteristic
 *
 * @ORM\Table(name="characteristic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CharacteristicRepository")
 */
class Characteristic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Characteristics\Armor",cascade={"all"}, orphanRemoval=true)
     */
    private $armor;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Characteristics\Chance", cascade={"all"}, orphanRemoval=true)
     */
    private $chance;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Characteristics\Life", cascade={"all"}, orphanRemoval=true)
     */
    private $life;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Characteristics\Strength",cascade={"all"}, orphanRemoval=true)
     */
    private $strength;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set armor
     *
     * @param \AppBundle\Entity\Characteristics\Armor $armor
     *
     * @return Characteristic
     */
    public function setArmor(\AppBundle\Entity\Characteristics\Armor $armor = null)
    {
        $this->armor = $armor;

        return $this;
    }

    /**
     * Get armor
     *
     * @return \AppBundle\Entity\Characteristics\Armor
     */
    public function getArmor()
    {
        return $this->armor;
    }

    /**
     * Set chance
     *
     * @param \AppBundle\Entity\Characteristics\Chance $chance
     *
     * @return Characteristic
     */
    public function setChance(\AppBundle\Entity\Characteristics\Chance $chance = null)
    {
        $this->chance = $chance;

        return $this;
    }

    /**
     * Get chance
     *
     * @return \AppBundle\Entity\Characteristics\Chance
     */
    public function getChance()
    {
        return $this->chance;
    }

    /**
     * Set life
     *
     * @param \AppBundle\Entity\Characteristics\Life $life
     *
     * @return Characteristic
     */
    public function setLife(\AppBundle\Entity\Characteristics\Life $life = null)
    {
        $this->life = $life;

        return $this;
    }

    /**
     * Get life
     *
     * @return \AppBundle\Entity\Characteristics\Life
     */
    public function getLife()
    {
        return $this->life;
    }

    /**
     * Set strength
     *
     * @param \AppBundle\Entity\Characteristics\Strength $strength
     *
     * @return Characteristic
     */
    public function setStrength(\AppBundle\Entity\Characteristics\Strength $strength = null)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * Get strength
     *
     * @return \AppBundle\Entity\Characteristics\Strength
     */
    public function getStrength()
    {
        return $this->strength;
    }
}
