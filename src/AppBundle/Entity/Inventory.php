<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Characteristic
 *
 * @ORM\Table(name="inventory")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InventoryRepository")
 */
class Inventory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Inventories\EquipmentInventory",cascade={"all"}, orphanRemoval=true)
     */
    private $equipment_invetory;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Inventories\ObjectInventory",cascade={"all"}, orphanRemoval=true)
     */
    private $object_invetory;
    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Inventories\PotionInventory",cascade={"all"}, orphanRemoval=true)
     */
    private $potion_invetory;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set equipmentInvetory
     *
     * @param \AppBundle\Entity\Inventories\EquipmentInventory $equipmentInvetory
     *
     * @return Inventory
     */
    public function setEquipmentInvetory(\AppBundle\Entity\Inventories\EquipmentInventory $equipmentInvetory = null)
    {
        $this->equipment_invetory = $equipmentInvetory;

        return $this;
    }

    /**
     * Get equipmentInvetory
     *
     * @return \AppBundle\Entity\Inventories\EquipmentInventory
     */
    public function getEquipmentInvetory()
    {
        return $this->equipment_invetory;
    }

    /**
     * Set objectInvetory
     *
     * @param \AppBundle\Entity\Inventories\ObjectInventory $objectInvetory
     *
     * @return Inventory
     */
    public function setObjectInvetory(\AppBundle\Entity\Inventories\ObjectInventory $objectInvetory = null)
    {
        $this->object_invetory = $objectInvetory;

        return $this;
    }

    /**
     * Get objectInvetory
     *
     * @return \AppBundle\Entity\Inventories\ObjectInventory
     */
    public function getObjectInvetory()
    {
        return $this->object_invetory;
    }

    /**
     * Set potionInvetory
     *
     * @param \AppBundle\Entity\Inventories\PotionInventory $potionInvetory
     *
     * @return Inventory
     */
    public function setPotionInvetory(\AppBundle\Entity\Inventories\PotionInventory $potionInvetory = null)
    {
        $this->potion_invetory = $potionInvetory;

        return $this;
    }

    /**
     * Get potionInvetory
     *
     * @return \AppBundle\Entity\Inventories\PotionInventory
     */
    public function getPotionInvetory()
    {
        return $this->potion_invetory;
    }
}
