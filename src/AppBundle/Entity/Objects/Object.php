<?php

namespace AppBundle\Entity\Objects;

use Doctrine\ORM\Mapping as ORM;

/**
 * Object
 *
 * @ORM\Table(name="object")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArmsRepository")
 */
class Object
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="described", type="text")
     */
    private $described;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="life_points", type="integer")
     */
    private $life_points;
    /**
     * @var
     * @ORM\Column(name="unique", type="boolean")
     */
    private $unique;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Object
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set described
     *
     * @param string $described
     *
     * @return Object
     */
    public function setDescribed($described)
    {
        $this->described = $described;

        return $this;
    }

    /**
     * Get described
     *
     * @return string
     */
    public function getDescribed()
    {
        return $this->described;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Object
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set nbItems
     *
     * @param integer $nbItems
     *
     * @return Object
     */
    public function setLifePoints($lifePoints)
    {
        $this->life_points = $lifePoints;

        return $this;
    }

    /**
     * Get nbItems
     *
     * @return integer
     */
    public function getLifePoints()
    {
        return $this->nb_items;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }
}
