<?php

namespace AppBundle\Entity\Objects;

use AppBundle\Entity\Dice;
use AppBundle\Entity\Hero;
use AppBundle\Services\ActionService;
use AppBundle\Services\HeroService;
use Doctrine\ORM\Mapping as ORM;
use PageBundle\Entity\Monster;
use PageBundle\Entity\MonsterPage;

/**
 * Object
 *
 * @ORM\Table(name="arms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArmsRepository")
 */
class Arms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="described", type="text")
     */
    private $described;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    private $hits;

    /**
     * @var int
     *
     * @ORM\Column(name="lifePoint", type="integer", nullable=true)
     */
    private $lifePoint;


    /**
     * @var int
     *
     * @ORM\Column(name="area_damage", type="boolean")
     */
    private $area_damage;


    /**
     * @var int
     *
     * @ORM\Column(name="dice", type="integer", nullable=true)
     */
    private $dice;


    /**
     * @var int
     *
     * @ORM\Column(name="dice_min", type="integer",nullable=true)
     */
    private $dice_min;

    /**
     * @var int
     *
     * @ORM\Column(name="dice_max", type="integer",nullable=true)
     */
    private $dice_max;
    /**
     * @var
     * @ORM\Column(name="unique", type="boolean")
     */
    private $unique;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Object
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set described
     *
     * @param string $described
     *
     * @return Object
     */
    public function setDescribed($described)
    {
        $this->described = $described;

        return $this;
    }

    /**
     * Get described
     *
     * @return string
     */
    public function getDescribed()
    {
        return $this->described;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Object
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     *
     * @return Object
     */
    public function setHits($hits)
    {
        $this->hits = $hits;

        return $this;
    }

    /**
     * Get hits
     *
     * @return int
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set lifePoint
     *
     * @param integer $lifePoint
     *
     * @return Object
     */
    public function setLifePoint($lifePoint)
    {
        $this->lifePoint = $lifePoint;

        return $this;
    }

    /**
     * Get lifePoint
     *
     * @return int
     */
    public function getLifePoint()
    {
        return $this->lifePoint;
    }

    /**
     * @return mixed
     */
    public function getUnique()
    {
        return $this->unique;
    }

    /**
     * @param mixed $unique
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAreaDamage()
    {
        return $this->area_damage;
    }

    /**
     * @param int $area_damage
     */
    public function setAreaDamage($area_damage)
    {
        $this->area_damage = $area_damage;
    }

    /**
     * @return int
     */
    public function getDice()
    {
        return $this->dice;
    }

    /**
     * @param int $dice
     */
    public function setDice($dice)
    {
        $this->dice = $dice;
    }

    /**
     * @return int
     */
    public function getDiceMin()
    {
        return $this->dice_min;
    }

    /**
     * @param int $dice_min
     */
    public function setDiceMin($dice_min)
    {
        $this->dice_min = $dice_min;
    }

    /**
     * @return int
     */
    public function getDiceMax()
    {
        return $this->dice_max;
    }

    /**
     * @param int $dice_max
     */
    public function setDiceMax($dice_max)
    {
        $this->dice_max = $dice_max;
    }

    public function attackNotZone(&$enemies,ActionService $actionService, HeroService &$heroService)
    {
        $return = '';
        //Page monster $enemies
        if($enemies instanceof  Monster)
        {

                //Attaque not  zone

                // not lance de de
                if(!$this->dice)
                {

                    //sans de
                    $dice  = $actionService->runDice(2,$this->dice_min, $this->dice_max);
                    $total = $dice->getTotal();

                    $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();

                    if($total <= $hability)
                    {
                        $enemies->setLife($enemies->getLife() - $this->hits);
                        $return = [
                            "message" => 'Vous avez infligé '.$this->hits.' Dégats',
                            "hits" => $this->hits
                        ];
                    }
                }
                else
                {
                    //avec dee
                    $dice  = $actionService->runDice(2,$this->dice_min, $this->dice_max);
                    $total = $dice->getTotal();

                    $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();

                    if($total <= $hability)
                    {
                        $dicAttack  = $actionService->runDice(1,$this->dice_min, $this->dice_max);
                        $totalAttack = $dicAttack->getTotal();

                        $enemies->setLife($enemies->getLife() - $totalAttack);
                        $return = [
                            "message" => 'Vous avez infligé '.$totalAttack.' Dégats',
                            "hits" => $totalAttack
                        ];
                    }
                    else
                    {
                        $return = "Vous avez manqué votre tir ";
                    }
                }
        }
        elseif ($enemies instanceof  Hero)
        {
            //Attaque not  zone

            if(!$this->dice)
            {
                // not lance de de
                $dice  = $actionService->runDice(2,$this->dice_min, $this->dice_max);
                $total = $dice->getTotal();

                $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();
                $heroLife = $heroService->getHero()->getCharacteristics()->getLife();

                if($total <= $hability)
                {
                    $heroLife->setCurrentPoint($heroLife->getCurrentPoint() - $this->hits);

                    $return = [
                        "message" => 'Vous avez infligé '.$this->hits.' Dégats',
                        "hits" => $this->hits
                    ];
                }
            }
            else
            {
                //lance de de
                $dice  = $actionService->runDice(2,$this->dice_min, $this->dice_max);
                $total = $dice->getTotal();

                $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();
                $heroLife = $heroService->getHero()->getCharacteristics()->getLife();

                if($total <= $hability)
                {
                    $dicAttack  = $actionService->runDice(1,$this->dice_min, $this->dice_max);
                    $totalAttack = $dicAttack->getTotal();

                    $heroLife->setCurrentPoint($heroLife->getCurrentPoint() - $totalAttack);
                    $return = [
                        "message" => 'Vous avez infligé '.$totalAttack.' Dégats',
                        "hits" => $totalAttack
                    ];
                }
            }
        }

        return $return;
    }

    public function attackZone(&$enemies,ActionService $actionService, HeroService &$heroService)
    {
        $return  = '';
        //Page monster $enemies
        if ($enemies instanceof Monster) {
            //Attaque not  zone

            if (!$this->dice) {
                // not lance de de
                $dice = $actionService->runDice(2, $this->dice_min, $this->dice_max);
                $total = $dice->getTotal();

                $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();

                if ($total <= $hability) {
                    $enemies->setLife($enemies->getLife() - $this->hits);
                    $enemies->setBasedLife($enemies->getBasedLife() - $this->hits);
                    $return = [
                        'message' => 'Vous avez attaqué '.$this->hits.'dégats',
                        'dice' => $total
                    ];                }
                else
                {
                    $return = [
                        'message' => 'Vous avez loupé votre attaque',
                        'dice' => $total
                    ];
                }
            } else {
                //lance de de
                $dice = $actionService->runDice(2, $this->dice_min, $this->dice_max);
                $total = $dice->getTotal();

                $hability = $heroService->getHero()->getCharacteristics()->getStrength()->getCurrentPoint();

                if ($total <= $hability) {
                    $dicAttack = $actionService->runDice(1, $this->dice_min, $this->dice_max);
                    $totalAttack = $dicAttack->getTotal();

                    $enemies->setLife($enemies->getLife() - $totalAttack);
                    $enemies->setBasedLife($enemies->getBasedLife() - $totalAttack);
                    $return = [
                        'message' => 'Vous avez attaqué '.$totalAttack.'dégats',
                        'dice' => $total
                    ];
                }
                else
                {
                    $return = [
                        'message' => 'Vous avez loupé votre attaque',
                        'dice' => $total
                    ];
                }
            }
        }

        return $return;


    }
}

