<?php

namespace AppBundle\Entity\Objects;

use Doctrine\ORM\Mapping as ORM;

/**
 * Object
 *
 * @ORM\Table(name="pills")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PillsRepository")
 */
class Pills
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="described", type="text")
     */
    private $described;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="heals", type="integer")
     */
    private $heals;

    /**
     * @var int
     *
     * @ORM\Column(name="lifePoint", type="integer")
     */
    private $lifePoint;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Object
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set described
     *
     * @param string $described
     *
     * @return Object
     */
    public function setDescribed($described)
    {
        $this->described = $described;

        return $this;
    }

    /**
     * Get described
     *
     * @return string
     */
    public function getDescribed()
    {
        return $this->described;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Object
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     *
     * @return Object
     */
    public function setheals($heals)
    {
        $this->heals = $heals;

        return $this;
    }

    /**
     * Get hits
     *
     * @return int
     */
    public function getheals()
    {
        return $this->heals;
    }

    /**
     * Set lifePoint
     *
     * @param integer $lifePoint
     *
     * @return Object
     */
    public function setLifePoint($lifePoint)
    {
        $this->lifePoint = $lifePoint;

        return $this;
    }

    /**
     * Get lifePoint
     *
     * @return int
     */
    public function getLifePoint()
    {
        return $this->lifePoint;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

}

