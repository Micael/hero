<?php

namespace AppBundle\Entity\Characteristics;

use AppBundle\Entity\Characteristic;
use AppBundle\Interfaces\CharacteristicsInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Life
 *
 * @ORM\Table(name="life")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LifeRepository")
 */
class Life  implements  CharacteristicsInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="startPoint", type="integer")
     */
    private $startPoint;

    /**
     * @var int
     *
     * @ORM\Column(name="currentPoint", type="integer")
     */
    private $currentPoint;

    /**
     * @var int
     *
     * @ORM\Column(name="priceImprovement", type="integer", nullable=true)
     */
    private $priceImprovement;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set startPoint
     *
     * @param integer $startPoint
     *
     * @return \AppBundle\Interfaces\Characteristic
     */
    public function setStartPoint($startPoint)
    {
        // TODO: Implement setStartPoint() method.

        $this->startPoint = $startPoint;
        $this->currentPoint = $startPoint;
        return $this;
    }

    /**
     * Get startPoint
     *
     * @return int
     */
    public function getStartPoint()
    {
        // TODO: Implement getStartPoint() method.
        return $this->startPoint;
    }

    /**
     * Set currentPoint
     *
     * @param integer $currentPoint
     *
     * @return \AppBundle\Interfaces\Characteristic
     */
    public function setCurrentPoint($currentPoint)
    {
        // TODO: Implement setCurrentPoint() method.

        $this->currentPoint = $currentPoint;
        return $this;
    }

    /**
     * Get currentPoint
     *
     * @return int
     */
    public function getCurrentPoint()
    {
        // TODO: Implement getCurrentPoint() method.

        return  $this->currentPoint;
    }

    /**
     * Set priceImprovement
     *
     * @param integer $priceImprovement
     *
     * @return \AppBundle\Interfaces\Characteristic
     */
    public function setPriceImprovement($priceImprovement)
    {
        // TODO: Implement setPriceImprovement() method.

        $this->priceImprovement = $priceImprovement;
        return $this;
    }

    /**
     * Get priceImprovement
     *
     * @return int
     */
    public function getPriceImprovement()
    {
        // TODO: Implement getPriceImprovement() method.

        return $this->priceImprovement;
    }

    public function  __toString()
    {
        return "".$this->currentPoint;
    }
}

