<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Hero
 *
 * @ORM\Table(name="hero")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HeroRepository")
 */
class Hero
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Characteristic", cascade={"all"}, orphanRemoval=true)
     */
    private $characteristics;

    /**
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Inventory",cascade={"all"}, orphanRemoval=true)
     */
    private $inventories;



    /**
     * @var string
     *
     * @ORM\Column(name="money", type="integer", length=255)
     */
    private $money;
    /**
     * @var User
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User",cascade={"all"}, orphanRemoval=true, inversedBy="hero")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Hero
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set characteristics
     *
     * @param \AppBundle\Entity\Characteristic $characteristics
     *
     * @return Hero
     */
    public function setCharacteristics(\AppBundle\Entity\Characteristic $characteristics = null)
    {
        $this->characteristics = $characteristics;

        return $this;
    }

    /**
     * Get characteristics
     *
     * @return \AppBundle\Entity\Characteristic
     */
    public function getCharacteristics()
    {
        return $this->characteristics;
    }

    /**
     * Set invetories
     *
     * @param \AppBundle\Entity\Inventory $invetories
     *
     * @return Hero
     */
    public function setInventories(\AppBundle\Entity\Inventory $inventories = null)
    {
        $this->inventories = $inventories;

        return $this;
    }

    /**
     * Get invetories
     *
     * @return \AppBundle\Entity\Inventory
     */
    public function getInventories()
    {
        return $this->inventories;
    }

    /**
     * Set money
     *
     * @param \int $money
     *
     * @return Hero
     */
    public function setMoney($money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * Get money
     *
     * @return \int
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Hero
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
