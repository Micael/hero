<?php

namespace AppBundle\Entity\Inventories;

use AppBundle\Interfaces\InventoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Object
 *
 * @ORM\Table(name="object_inventory")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ObjectInventoryRepository")
 */
class ObjectInventory implements InventoryInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="place", type="integer", nullable=true)
     */
    private $place;

    /**
     * @var array
     * type serialize array !
     * @ORM\Column(name="objects", type="array")
     */
    private $objects;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param integer $place
     *
     * @return Object
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return int
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set objects
     *
     * @param array $objects
     *
     * @return Object
     */
    public function setObjects($objects)
    {
        $this->objects = $objects;

        return $this;
    }

    /**
     * Get objects
     *
     * @return array
     */
    public function getObjects()
    {
        return $this->objects;
    }

    public function addObject($object){

        $this->objects[$object->getId()][] = $object;
        return $this;
    }
//    public function removeObject($object){
//
//        unset($this->objects[$object->getId()]);
//        return $this;
//    }

    public function removeObject($object){
        if(isset($this->objects[$object->getId()]))
        {
//            $arm = $this->objects[$object->getId()][0];
//            if(!$arm->getUnique())
//            {
//
//                if($arm->getLifePoint() > 0 )
//                {
//                    $this->objects[$object->getId()][0]->setLifePoint($arm->getLifePoint() -1 );
//                }else
//                {
                    unset($this->objects[$object->getId()]);

//                }
//            }
        }
        return $this;
    }
}

