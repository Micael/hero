<?php

namespace AppBundle\Entity;

class Dice
{
    private $number;
    private $results  = array();

    public  function  __construct($number)
    {
        $this->number = $number;
    }

    public function addResult($result)
    {
        $this->results[] = $result;
    }

    public function getTotal()
    {
        $total = 0;

        foreach($this->results as $result)
        {
            $total += $result;
        }

        return $total;
    }

    public  function firstRound()
    {
        return $this->results[0];
    }

    public function getRolled($round)
    {
        return $this->results[$round];
    }
}