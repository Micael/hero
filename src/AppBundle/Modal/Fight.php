<?php

namespace AppBundle\Modal;


use AppBundle\Entity\Objects\Arms;
use AppBundle\Services\ActionService;
use AppBundle\Services\HeroService;

class Fight
{
    private $arms_autorized;
    private $object_autorized;
    private $fightType;
    private $monsters;

    /**
     * @return mixed
     */
    public function getArmsAutorized()
    {
        return $this->arms_autorized;
    }



    /**
     * @param mixed $arms_autorized
     */
    public function setArmsAutorized($arms_autorized)
    {
        $this->arms_autorized = $arms_autorized;
    }

    /**
     * @return mixed
     */
    public function getObjectAutorized()
    {
        return $this->object_autorized;
    }

    /**
     * @param mixed $object_autorized
     */
    public function setObjectAutorized($object_autorized)
    {
        $this->object_autorized = $object_autorized;
    }


    /**
     * @return mixed
     */
    public function getFightType()
    {
        return $this->fightType;
    }

    /**
     * @param mixed $fightType
     */
    public function setFightType($fightType)
    {
        $this->fightType = $fightType;
    }

    /**
     * @return mixed
     */
    public function getMonsters()
    {
        return $this->monsters;
    }

    /**
     * @param mixed $monsters
     */
    public function setMonsters($monsters)
    {
        $this->monsters = $monsters;
    }

    public function getMonstersToFight()
    {
        return $this->getMonsters()[0];
    }

    public function heroAttackMonster($attack = 2)
    {

        $pageMonster  = $this->getMonstersToFight();
        $monster = $this->getMonstersToFight()->getMonster();
        $monster->setLife($monster->getLife() - $attack);

        if($monster->getLife() <= 0)
        {
            $pageMonster->setQte($pageMonster->getQte() - 1);
            if($pageMonster->getQte() <= 0)
            {
                    unset($this->monsters[0]);
            }else
            {
                $monster->setLife($monster->getBasedLife());
            }

        }

        return $this;
    }

    public function heroAttackMonsterWhitArms(Arms $arms, ActionService $service,HeroService &$heroService)
    {
        $pageMonster  = $this->getMonstersToFight();
        $monster = $this->getMonstersToFight();

        if($arms->getAreaDamage())
        {
            foreach ($this->getMonsters() as $monster)
            {
                $monster = $monster->getMonster();

                 $attackResult = $arms->attackZone($monster, $service, $heroService);

                if($monster->getLife() <= 0)
                {

                    if($pageMonster->getMonster()->getBasedLife() <= 0)
                    {
                        unset($this->monsters[0]);

                    }
                    else
                    {
                        $pageMonster->setQte($pageMonster->getQte() - 1);
                        if($pageMonster->getQte() <= 0)
                        {
                            unset($this->monsters[0]);
                        }else
                        {

                            $monster->setLife($monster->getBasedLife());
                        }

                    }
                }
            }

        }else{

            $monster = $monster->getMonster();

            $attackResult =  $arms->attackNotZone($monster, $service, $heroService);

            if($monster->getLife() <= 0)
            {
                $pageMonster->setQte($pageMonster->getQte() - 1);
                if($pageMonster->getQte() <= 0)
                {
                    unset($this->monsters[0]);
                }else
                {
                    $monster->setLife($monster->getBasedLife());
                }

            }
        }

        //delete lif of arms
        $armsHeroObjects = $heroService->getHero()->getInventories()->getEquipmentInvetory()->getObjects();

        if(isset($armsHeroObjects[$arms->getId()]))
        {
            $armsHero = $armsHeroObjects[$arms->getId()][0];

            if($armsHero->getLifePoint())
            {

                if($armsHero->getLifePoint() <= 0 )
                {
                    $heroService->getHero()->getInventories()->getEquipmentInvetory()->removeObject($arms);
                }

                $armsHero->setLifePoint($armsHero->getLifePoint() - 1);
            }
        }

        return $attackResult;

    }

    public function monsterAttackHero(ActionService $service,HeroService &$heroService)
    {
        $monster = $this->getMonstersToFight();

        $arms = $monster->getMonster()->getArm();
        $return = [];
        if($arms)
        {

            $monster = $heroService->getHero();

            $attackResult = $arms->attackNotZone($monster, $service, $heroService);


            $life = $monster->getCharacteristics()->getLife();

            if($life->getCurrentPoint() <= 0)
            {
                return  [
                    'result' => false,
                    'lost' => true,
                    'message' => "Vous avez perdu le combat"
                ];


            }else
            {
                return  [
                    'result' => true,
                    'attackResult' => $attackResult,
                    'message' => $attackResult['message']

                ];
            }
        }else
        {
            $resulAttackMonster = $service->runDice(2,1,6)->getTotal();
            $heroResult = $service->runDice(2,1,6)->getTotal();

            if($heroResult < $resulAttackMonster)
            {
                //monster attack
                $life = $heroService->getHero()->getCharacteristics()->getLife();
                $life->setCurrentPoint($life->getCurrentPoint() - 2);

                if($life->getCurrentPoint() <= 0)
                {
                    return  [
                        'result' => false,
                        'lost' => true,
                        'message' => "Vous avez perdu le combat"
                    ];


                }else
                {
                    return  [
                        'result' => true,
                        'message' => "Vous avez été attaqué de 2"
                    ];
                }
            }
        }

        return $return;

    }

    public function fightWin()
    {
        if(empty($this->monsters))
        {
            return true;
        }
        else{
            false;
        }
    }


}